# import the necessary packages
from imutils.video import VideoStream
from imutils import face_utils
import imutils
import time
import dlib
import cv2
import numpy as np
from predict import predict


def get_message(name):
    message = ""
    if name == 0:
        message = "between "
    elif name == 1:
        message = "between 0 and 10 "
    elif name == 2:
        message = "between 10 and 20 "
    elif name == 3:
        message = "between 20 and 30"
    elif name == 4:
        message = "between 30 and 40"
    elif name == 5:
        message = "between 40 and 55"
    elif name == 6:
        message = "between 55 and 65"
    elif name == 7:
        message = "between 65 and 90"

    return message


pre = 'shape_predictor_68_face_landmarks.dat'
detector = dlib.get_frontal_face_detector()

predictor = dlib.shape_predictor(pre)

vs = VideoStream(0).start()
time.sleep(2.0)
font = cv2.FONT_HERSHEY_PLAIN

while True:
    frame = vs.read()
    frame = imutils.resize(frame, width=256)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale frame
    rects = detector(gray, 0)

    # loop over the face detections
    for rect in rects:

        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        # loop over the (x, y)-coordinates for the facial landmarks
        # and draw them on the image
        a = []
        for (x, y) in shape:
            cv2.circle(frame, (x, y), 1, (50, 50, 50), -1)
            a.append(np.float32(x))
            a.append(np.float32(y))
        a = np.array(a)
        name = predict(a)
        message = get_message(name)
        cv2.putText(frame, message, (0, 10), font, 1, (200, 255, 155), 2)
        # print message

    # show the frame
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break


cv2.destroyAllWindows()
vs.stop()


