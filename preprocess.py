import cv2
import os
import re
import numpy as np
import csv
from sklearn.model_selection import train_test_split
import random
from face_map import *

IMG_WIDTH = 256
IMG_HEIGHT = 256


def remove_txt(dirname):
    '''
    :param dirname: name of the directory
    :return:  walks through directory and removes texts
    '''
    for cur, _dirs, files in os.walk(dirname):
        head, tail = os.path.split(cur)
        while head:
            head, _tail = os.path.split(head)
        for f in files:
            if ".txt" in f:
                os.remove("data/faces/" + tail + "/" + f)


def convert_graysale(f):
    '''
    :param f: file name
    :return: returns the grayscaled and scaled version of the image
    '''
    image = cv2.imread(f)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    resized_image = cv2.resize(gray_image, (IMG_HEIGHT, IMG_WIDTH))
    cv2.imwrite(f, resized_image)
    # return resized_image


def grayscale_resize_save(f):
    '''
    :param f: file path
    :return: grayscales all images in the directory
    '''
    count = 0
    for cur, _dirs, files in os.walk(f):
        head, tail = os.path.split(cur)
        while head:
            head, _tail = os.path.split(head)

        for f in files:
            if ".jpg" in f:
                path = "data/faces/" + tail + "/" + f
                convert_graysale(path)
                print count, "converted ", path
                count += 1

def landmark_save(f):
    '''
    :param f: file path
    :return:
    '''
    class_labels = []
    class_labels_num = np.zeros(9)
    file_name = "data/data_land_256.csv"
    my_file = open(file_name, 'wb')
    writer2 = csv.writer(my_file)
    count = 0
    for cur, _dirs, files in os.walk(f):
        head, tail = os.path.split(cur)
        while head:
            head, _tail = os.path.split(head)

        for f in files:
            if ".jpg" in f:
                path = "data/faces/" + tail + "/" + f
                image_array = give_facemap(path)
                image_array = image_array
                if image_array is None:
                    continue
                image_array = image_array.tolist()
                image_array = map_to_bin(image_array)
                image_array = np.array(image_array)
                image_array = image_array.reshape(65536)

                class_num = find_class("data/class_label", f)
                if class_num not in class_labels:
                    class_labels.append(class_num)
                class_labels_num[class_labels.index(class_num)] += 1
                one_hot = np.eye(9)[class_num]
                one_hot = one_hot.tolist()

                csv_row_1 = image_array.tolist() + one_hot
                print (count), len(csv_row_1), class_num, one_hot
                if len(csv_row_1) != 65545:
                    continue
                writer2.writerow(csv_row_1)
                my_file.flush()
                count += 1
    my_file.close()
    for i in range(len(class_labels)):
        print class_num[i], "-->", class_labels_num[i]


def generate_train_txt(f):
    '''
    :param f: file path
    :return:
    '''
    file_txt = open("data.txt", "w")
    count = 0
    class_labels = []
    class_labels_num = np.zeros(9)

    for cur, _dirs, files in os.walk(f):
        head, tail = os.path.split(cur)
        while head:
            head, _tail = os.path.split(head)
        for f in files:
            if ".jpg" in f:
                path = "data/faces/" + tail + "/" + f
                class_num = find_class("data/class_label", f)
                string_written = path + " "+str(class_num)
                print count,string_written
                file_txt.writelines(string_written+'\n')
                count +=1
                if class_num not in class_labels:
                    class_labels.append(class_num)
                class_labels_num[class_labels.index(class_num)] += 1

    file_txt.close()

    for i in range(len(class_labels)):
        print class_labels[i], "-->", class_labels_num[i]



def make_train_test( train_file, test_file, test_size, random_state):
    '''
    :param data_file: file containing full data thats waiting to be shuffeled and broken to train and test
    :param train_file: training file to be created
    :param test_file:  testing file to be created
    :param test_size: % of test data
    :param random_state: randomizing input
    :return:
    '''
    with open('data.txt') as f:
        data = f.readlines()
    random.shuffle(data)
    random.shuffle(data)
    train, test = train_test_split(data, test_size=test_size, random_state=random_state)
    random.shuffle(train)
    random.shuffle(test)

    X = np.asarray(data)
    Y = np.asarray(train)
    z = np.asarray(test)
    print "prev shape : ", X.shape, "train size ", Y.shape, ":test size", z.shape

    file_txt = open("train.txt", "w")
    for i in range(len(train)):
        file_txt.write(train[i])
    file_txt.close()

    file_txt2 = open("test.txt", "w")
    for i in range(len(test)):
        file_txt2.write(test[i])
    file_txt2.close()


def find_class(param, file_name):
    '''
    :param param: folder where the data is in
    :param file_name: the name of the image
    :return: number that represents a class that image represents given file name and csv that contains the data
    '''
    for cur, _dirs, files in os.walk(param):
        head, tail = os.path.split(cur)
        while head:
            head, _tail = os.path.split(head)
        for f in files:
            if ".csv" in f:
                file_path = param + "/" + f
                train_data = list(csv.reader(open(file_path), delimiter='\t'))
                for i in range(len(train_data)):
                    image_name = train_data[i][2] + "." + train_data[i][1]
                    if image_name in file_name:
                        print train_data[i][3]
                        class_label = get_classification(train_data[i][3])
                        if class_label is None:
                            return 0
                        else:
                            return class_label


def statistics(file_data, file_test, file_train):
    '''
    :param f: file path
    :return: prints class number of images statistics
    '''
    file_data = list(csv.reader(open(file_data)))
    # file_test = list(csv.reader(open(file_test)))
    # file_train = list(csv.reader(open(file_train)))

    file_data = np.array(file_data)
    # file_test = np.array(file_data)
    # file_train = np.array(file_train)


def get_classification(input):
    '''
    :param input: tuple or string representing age
    :return: number representing one of the age classes
    '''
    print "input ", input
    if input is None:
        return 0
    class_num = 0
    num = eval(input)
    if isinstance(num, tuple):
        class_num = (num[0] + num[1]) / 2
    elif isinstance(num, int):
        class_num = num

    if 0 < class_num < 10:
        return 1
    elif 10 <= class_num < 20:
        return 2
    elif 20 <= class_num < 30:
        return 3
    elif 30 <= class_num < 40:
        return 4
    elif 40 <= class_num < 55:
        return 5
    elif 55 <= class_num < 65:
        return 6
    elif 65 <= class_num < 90:
        return 7
    else:
        return 0


def binarize_on_landmark(f):
    count = 0
    mycsv = 'data/data_landmark.csv'
    data = np.array(list(csv.reader(open('data/data_landmark.csv'), delimiter=',')))
    print data[0][1]
    # for i in range(len(data)):
    #     class_num = find_class("data/class_label", data[i][0])
    #     # print data[i][0],class_num
    #     landmark_array = data[0][1]
    #     # landmark_array = [map(float, landmark_array[i]) for i in range(len(landmark_array))]
    #     print len(landmark_array)


def get_training_data(f):
    train_data = list(csv.reader(open(f)))
    random.shuffle(train_data)
    train_data = np.array(train_data)
    return train_data


def get_testing_data(f):
    test_data = list(csv.reader(open(f)))
    random.shuffle(test_data)
    test_data = np.array(test_data)
    return test_data


def splitter():
    pass


# remove_txt("data/faces")
# landmark_save("data/")
# make_train_test( "data/data_train_land.csv", "data/data_test_land.csv", test_size=0.3, random_state=40)
# statistics("data/data22.csv", "data/data_test.csv", "data/data_train.csv")
grayscale_resize_save('data/')
# binarize_on_landmark('data/')
# generate_train_txt('data/')
# splitter()

# a = find_class("data/class_label",'data/faces/37303189@N08/coarse_tilt_aligned_face.90.11166447045_224fc3cbdc_o.jpg')
# print a
